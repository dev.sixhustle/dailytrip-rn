import React, {useEffect} from 'react';
import Colors from '~/assets/Colors';
import LottieView from 'lottie-react-native';
import _ from 'lodash';

import {View, Text, StyleSheet} from 'react-native';

const AnimationScreen = props => {
  const {navigation} = props;

  useEffect(() => {
    _.delay(() => {
      navigation.popToTop();
      navigation.pop();
    }, 5500);
  }, []);

  return (
    <View style={styles.container}>
      <LottieView
        loop
        autoPlay
        source={require('../../animation/9482-select-your-trip.json')}
      />
      <Text style={styles.text}>당신만의 여행을 찾고 있습니다 :)</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingBottom: 160,
  },

  text: {
    fontSize: 20,
    fontFamily: 'S-CoreDream-3Light',
  },
});

export default AnimationScreen;
