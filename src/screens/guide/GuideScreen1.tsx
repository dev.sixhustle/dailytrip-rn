import React from 'react';
import {View, Dimensions, SafeAreaView} from 'react-native';
import {Text, Button} from 'react-native-elements';

const {width: deviceWidth} = Dimensions.get('window');
const statusBarWidth = deviceWidth / 5 - 2;

const GuideScreen1 = props => {
  console.log('[GuideScreen1] props', props);

  const {navigation} = props;

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
      <Button
        title="다음으로"
        style={{position: 'absolute', right: 16, top: 16}}
        type="clear"
        titleStyle={{fontSize: 18}}
        onPress={() => navigation.navigate('secondGuideScreen')}
      />
      <View
        style={{
          flex: 1,
          marginTop: 80,
          marginLeft: 40,
        }}>
        <Text h3 style={{fontFamily: 'BlackHanSans-Regular'}}>
          {`DailyTrip은\n`}
          <Text h4>{`나라, 기간, 인원, 예산에 따라\n`}</Text>
          <Text h4>...</Text>
        </Text>
      </View>
      <View style={{flex: 1}} />
      <StatusBar />
    </SafeAreaView>
  );
};

const StatusBar = () => (
  <View
    style={{
      position: 'absolute',
      bottom: 80,
      width: '100%',
      height: 16,
      flexDirection: 'row',
      justifyContent: 'space-between',
    }}>
    <View
      style={{
        width: statusBarWidth,
        height: 16,
        backgroundColor: '#bebebe',
      }}></View>
    <View
      style={{
        width: statusBarWidth,
        height: 16,
        backgroundColor: '#e0e0e0',
      }}></View>
    <View
      style={{
        width: statusBarWidth,
        height: 16,
        backgroundColor: '#e0e0e0',
      }}></View>
    <View
      style={{
        width: statusBarWidth,
        height: 16,
        backgroundColor: '#e0e0e0',
      }}></View>
    <View
      style={{
        width: statusBarWidth,
        height: 16,
        backgroundColor: '#e0e0e0',
      }}></View>
  </View>
);

export default GuideScreen1;
