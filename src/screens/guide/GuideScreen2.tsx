import React, {useState} from 'react';
import {View, Dimensions, SafeAreaView} from 'react-native';
import {Text, Button, SearchBar} from 'react-native-elements';

const {width: deviceWidth} = Dimensions.get('window');
const statusBarWidth = deviceWidth / 5 - 2;

const GuideScreen2 = ({navigation}) => {
  const [searchText, setSearchText] = useState('');

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
      <Button
        title="다음으로"
        style={{position: 'absolute', right: 16, top: 16}}
        type="clear"
        titleStyle={{fontSize: 18}}
        onPress={() => navigation.navigate('thirdGuideScreen')}
      />
      <View
        style={{
          flex: 1,
          marginTop: 80,
          marginHorizontal: 40,
        }}>
        <Text h2 style={{fontFamily: 'BlackHanSans-Regular'}}>
          {`어느 나라를\n여행하시나요?`}
        </Text>
      </View>
      <View style={{flex: 1, paddingHorizontal: 40}}>
        <SearchBar
          lightTheme
          value={searchText}
          onChangeText={setSearchText}
          autoCapitalize={false}
          autoCapitalize="none"
        />
      </View>
      <StatusBar />
    </SafeAreaView>
  );
};

const StatusBar = () => (
  <View
    style={{
      position: 'absolute',
      bottom: 80,
      width: '100%',
      height: 16,
      flexDirection: 'row',
      justifyContent: 'space-between',
    }}>
    <View
      style={{
        width: statusBarWidth,
        height: 16,
        backgroundColor: '#bebebe',
      }}></View>
    <View
      style={{
        width: statusBarWidth,
        height: 16,
        backgroundColor: '#9e9e9e',
      }}></View>
    <View
      style={{
        width: statusBarWidth,
        height: 16,
        backgroundColor: '#e0e0e0',
      }}></View>
    <View
      style={{
        width: statusBarWidth,
        height: 16,
        backgroundColor: '#e0e0e0',
      }}></View>
    <View
      style={{
        width: statusBarWidth,
        height: 16,
        backgroundColor: '#e0e0e0',
      }}></View>
  </View>
);

export default GuideScreen2;
