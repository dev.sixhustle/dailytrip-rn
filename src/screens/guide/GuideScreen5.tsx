import React, {useState, useEffect} from 'react';
import {View, Dimensions, SafeAreaView, Picker} from 'react-native';
import {Text, Button, SearchBar} from 'react-native-elements';

const {width: deviceWidth} = Dimensions.get('window');
const statusBarWidth = deviceWidth / 5 - 2;

const days = [
  '100',
  '150',
  '200',
  '250',
  '300',
  '350',
  '400',
  '450',
  '500',
  '550',
];

const GuideScreen5 = ({navigation}) => {
  const [selectedTravelCost, setSelectedTravelCost] = useState('300');

  useEffect(() => {
    console.log('[selectedTravelCost]', selectedTravelCost);
  }, [selectedTravelCost]);

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
      <Button
        title="다음으로"
        style={{position: 'absolute', right: 16, top: 16}}
        type="clear"
        titleStyle={{fontSize: 18}}
        onPress={() => {
          navigation.navigate('loadingScreen');
        }}
      />
      <View
        style={{
          flex: 1,
          marginTop: 80,
          marginHorizontal: 40,
        }}>
        <Text h2 style={{fontFamily: 'BlackHanSans-Regular'}}>
          {`얼마의 예산으로 여행하시나요?`}
        </Text>
      </View>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
        }}>
        <Picker
          selectedValue={String(selectedTravelCost)}
          onValueChange={setSelectedTravelCost}
          style={{
            height: 'auto',
            width: 160,
            fontFamily: 'BlackHanSans-Regular',
          }}
          mode="dialog"
          itemStyle={{fontSize: 24, fontFamily: 'BlackHanSans-Regular'}}>
          {days.map(day => {
            return <Picker.Item key={day} label={day} value={day} />;
          })}
        </Picker>
      </View>
      <PricesView
        selectedTravelCost={selectedTravelCost}
        setSelectedTravelCost={setSelectedTravelCost}
      />
      <StatusBar />
    </SafeAreaView>
  );
};

const prices = [1, 5, 10, 30, 50, 100];

const PricesView = ({selectedTravelCost, setSelectedTravelCost}) => (
  <View
    style={{
      width: '100%',
      height: 24,
      flexDirection: 'row',
      justifyContent: 'flex-end',
      marginBottom: 80,
      paddingRight: 16,
    }}>
    {prices.map(price => (
      <PriceButton
        price={price}
        selectedTravelCost={selectedTravelCost}
        setSelectedTravelCost={setSelectedTravelCost}
      />
    ))}
  </View>
);

const PriceButton = ({price, selectedTravelCost, setSelectedTravelCost}) => (
  <Button
    containerStyle={{
      width: 56,
      height: 40,
      marginRight: 4,
    }}
    buttonStyle={{
      margin: 0,
      padding: 0,
      backgroundColor: '#aeaeae',
    }}
    title={`+${price}만`}
    titleStyle={{fontSize: 12}}
    onPress={() =>
      setSelectedTravelCost(
        String(parseInt(selectedTravelCost) + parseInt(price)),
      )
    }
  />
);

const StatusBar = () => (
  <View
    style={{
      position: 'absolute',
      bottom: 80,
      width: '100%',
      height: 16,
      flexDirection: 'row',
      justifyContent: 'space-between',
    }}>
    <View
      style={{
        width: statusBarWidth,
        height: 16,
        backgroundColor: '#bebebe',
      }}></View>
    <View
      style={{
        width: statusBarWidth,
        height: 16,
        backgroundColor: '#9e9e9e',
      }}></View>
    <View
      style={{
        width: statusBarWidth,
        height: 16,
        backgroundColor: '#7f7f7f',
      }}></View>
    <View
      style={{
        width: statusBarWidth,
        height: 16,
        backgroundColor: '#5e5e5e',
      }}></View>
    <View
      style={{
        width: statusBarWidth,
        height: 16,
        backgroundColor: '#404040',
      }}></View>
  </View>
);

export default GuideScreen5;
