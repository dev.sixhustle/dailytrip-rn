import React, {useState} from 'react';
import {View, Dimensions, SafeAreaView, Picker} from 'react-native';
import {Text, Button, SearchBar} from 'react-native-elements';

const {width: deviceWidth} = Dimensions.get('window');
const statusBarWidth = deviceWidth / 5 - 2;

const days = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];

const GuideScreen4 = ({navigation}) => {
  const [selectedTravelDay, setSelectedTravelDay] = useState('5');

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
      <Button
        title="다음으로"
        style={{position: 'absolute', right: 16, top: 16}}
        type="clear"
        titleStyle={{fontSize: 18}}
        onPress={() => navigation.navigate('fifthGuideScreen')}
      />
      <View
        style={{
          flex: 1,
          marginTop: 80,
          marginHorizontal: 40,
        }}>
        <Text h2 style={{fontFamily: 'BlackHanSans-Regular'}}>
          {`몇 명의 친구들과\n여행 하시나요?`}
        </Text>
      </View>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
        }}>
        <Picker
          selectedValue={selectedTravelDay}
          onValueChange={setSelectedTravelDay}
          style={{
            height: 'auto',
            width: 160,
            fontFamily: 'BlackHanSans-Regular',
          }}
          mode="dialog"
          itemStyle={{fontSize: 24, fontFamily: 'BlackHanSans-Regular'}}>
          {days.map(day => {
            return <Picker.Item key={day} label={day} value={day} />;
          })}
        </Picker>
      </View>
      <StatusBar />
    </SafeAreaView>
  );
};

const StatusBar = () => (
  <View
    style={{
      position: 'absolute',
      bottom: 80,
      width: '100%',
      height: 16,
      flexDirection: 'row',
      justifyContent: 'space-between',
    }}>
    <View
      style={{
        width: statusBarWidth,
        height: 16,
        backgroundColor: '#bebebe',
      }}></View>
    <View
      style={{
        width: statusBarWidth,
        height: 16,
        backgroundColor: '#9e9e9e',
      }}></View>
    <View
      style={{
        width: statusBarWidth,
        height: 16,
        backgroundColor: '#7f7f7f',
      }}></View>
    <View
      style={{
        width: statusBarWidth,
        height: 16,
        backgroundColor: '#5e5e5e',
      }}></View>
    <View
      style={{
        width: statusBarWidth,
        height: 16,
        backgroundColor: '#e0e0e0',
      }}></View>
  </View>
);

export default GuideScreen4;
