import React, {useRef, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  ScrollView,
} from 'react-native';
import {ISplashScreen} from '~/interfaces/Interfaces';
import {
  SearchBar,
  Button as ButtonElement,
  withBadge,
} from 'react-native-elements';
import {Icon} from 'react-native-elements';
import Carousel from 'react-native-snap-carousel';
import _ from 'lodash';
import Colors from '~/assets/Colors';

const {width: deviceWidth, height: deviceHeight} = Dimensions.get('window');

const ENTRIES1 = [
  {
    title: 'Beautiful and dramatic Antelope Canyon',
    subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
    illustration: 'https://i.imgur.com/UYiroysl.jpg',
  },
  {
    title: 'Earlier this morning, NYC',
    subtitle: 'Lorem ipsum dolor sit amet',
    illustration: 'https://i.imgur.com/UPrs1EWl.jpg',
  },
  {
    title: 'White Pocket Sunset',
    subtitle: 'Lorem ipsum dolor sit amet et nuncat ',
    illustration: 'https://i.imgur.com/MABUbpDl.jpg',
  },
  {
    title: 'Acrocorinth, Greece',
    subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
    illustration: 'https://i.imgur.com/KZsmUi2l.jpg',
  },
  {
    title: 'The lone tree, majestic landscape of New Zealand',
    subtitle: 'Lorem ipsum dolor sit amet',
    illustration: 'https://i.imgur.com/2nCt3Sbl.jpg',
  },
  {
    title: 'Middle Earth, Germany',
    subtitle: 'Lorem ipsum dolor sit amet',
    illustration: 'https://i.imgur.com/lceHsT6l.jpg',
  },
];

const TemplatePreview = (props: ISplashScreen) => {
  /********************************************
    VARIABLE
  *********************************************/
  const {navigation} = props;
  const [searchMode, setSearchMode] = useState<boolean>(false);
  const [searchText, setSearchText] = useState<string>();
  const [recentlySearchKeywordList, setRecentlySearchKeywordList] = useState<
    Array<string>
  >(['주상절리', '신창풍차해안도로', '게스트하우스', '데일리커밋렌터카']);

  /********************************************
    USER FUNCTION
  *********************************************/
  const adjustKeywordList = (keyword: string) => {
    const newRecentlySearchKeywordList = _.pull(
      recentlySearchKeywordList,
      keyword,
    );

    setRecentlySearchKeywordList(newRecentlySearchKeywordList);
  };

  /********************************************
    MAIN LAYOUT (RENDER)
  ********************************************/
  return (
    <View style={styles.container}>
      <View style={styles.topContainer}>
        <FilterView />
        <CustomSearchBar
          searchText={searchText}
          setSearchText={setSearchText}
          setSearchMode={setSearchMode}
        />
      </View>
      <ScrollView
        keyboardDismissMode="on-drag"
        keyboardShouldPersistTaps="always"
        contentContainerStyle={{paddingBottom: 80}}>
        {searchMode ? (
          <SearchView
            setSearchText={setSearchText}
            recentlySearchKeywordList={recentlySearchKeywordList}
            adjustKeywordList={adjustKeywordList}
          />
        ) : (
          <View style={styles.tripViewParentContainer}>
            <TripItem />
            <TripItem />
            <TripItem />
          </View>
        )}
      </ScrollView>
      <MyTripBoxView />
    </View>
  );
};

/********************************************
    SUB LAYOUT - TOP HEADER
*********************************************/
const FilterView = () => (
  <View style={[styles.filterContainer, styles.shadow]}>
    <ButtonElement type="clear" style={{height: 32}} icon={FilterIcon} />
    <FilterItem title="3일" />
    <FilterItem title="50만원" />
  </View>
);

const FilterIcon = () => (
  <Icon type="foundation" name="filter" size={20} color={Colors.blue1} />
);

const FilterItem = ({title = 'FILTER'}) => (
  <ButtonElement
    iconRight
    icon={deleteIcon}
    type="outline"
    title={title}
    containerStyle={{height: 28, marginRight: 8}}
    titleStyle={{
      fontFamily: 'Arita-dotum-SemiBold_OTF',
      fontSize: 10,
    }}
  />
);

const deleteIcon = () => (
  <Icon
    type="octicon"
    name="x"
    size={12}
    color={Colors.blue1}
    containerStyle={{marginLeft: 8}}
  />
);

const CustomSearchBar = (props: any) => {
  const {searchText, setSearchText, setSearchMode} = props;

  return (
    <SearchBar
      round
      platform="ios"
      placeholder="여행지를 검색해보세요"
      value={searchText}
      onChangeText={setSearchText}
      containerStyle={styles.searchBarContainer}
      inputContainerStyle={[styles.inputContainer, styles.shadow]}
      inputStyle={styles.input}
      leftIconContainerStyle={styles.inputIconContainer}
      rightIconContainerStyle={styles.inputIconContainer}
      returnKeyType="search"
      cancelButtonProps={{
        buttonTextStyle: {
          fontFamily: 'BlackHanSans-Regular',
          fontSize: 16,
          marginTop: 4,
          color: 'white',
        },
      }}
      onFocus={() => setSearchMode(true)}
      onCancel={() => setSearchMode(false)}
      onEndEditing={() => setSearchMode(false)}
    />
  );
};

/********************************************
    SUB LAYOUT - MAIN CONTENT
*********************************************/
const TripItem = () => {
  const sliderWidth = deviceWidth / 1.0525;
  const itemWidth = deviceWidth / 1.0525;

  const carouselRef = useRef(null);
  const [currentIdx, setCurrentIdx] = useState(1);

  const _renderItem = ({item, index}) => {
    return <Image style={{flex: 1}} source={{uri: item.illustration}} />;
  };

  console.log('[TemplatePreview] TripView carouselRef', carouselRef);

  return (
    <View style={[styles.tripContainer, styles.shadow]}>
      <CarouselCountView currentIdx={currentIdx} maxLength={ENTRIES1.length} />
      <TripItemTitle />
      <Carousel
        ref={carouselRef}
        layout={'stack'}
        layoutCardOffset={18}
        data={ENTRIES1}
        renderItem={_renderItem}
        sliderWidth={sliderWidth}
        itemWidth={itemWidth}
        onSnapToItem={index => setCurrentIdx(index + 1)}
      />
      <ItemInfo />
    </View>
  );
};

const TripItemTitle = ({title = '50만원으로 제주 뽀개기 !!'}) => (
  <View style={styles.tripTitleContainer}>
    <Text style={styles.tripViewTitle} numberOfLines={1} ellipsizeMode="tail">
      {title}
    </Text>
  </View>
);

const CarouselCountView = ({currentIdx = 1, maxLength = 1}) => (
  <View style={styles.carouselCountContainer}>
    <Text
      style={styles.carouselCountText}>{`${currentIdx} / ${maxLength}`}</Text>
  </View>
);

const ItemInfo = () => (
  <View style={styles.detailContainer}>
    <View style={{flexDirection: 'row'}}>
      <DetailButton
        title="123"
        iconName="like2"
        iconType="antdesign"
        iconColor={Colors.blue1}
        textColor={Colors.blue1}
        borderColor={Colors.blue1}
      />
      <DetailButton
        title="123"
        iconName="dislike2"
        iconType="antdesign"
        iconColor="red"
        textColor="red"
        borderColor="red"
      />
    </View>
    <View style={{flexDirection: 'row'}}>
      <DetailButton
        title="3"
        iconName="calendar"
        iconType="antdesign"
        iconColor="grey"
      />
      <DetailButton
        title="500,000"
        iconName="money"
        iconType="font-awesome"
        iconColor="grey"
      />
    </View>
  </View>
);

const MyTripBoxView = () => (
  <View style={styles.myTripBoxContainer}>
    <DayButton day="1" />
    <DayButton day="2" />
    <DayButton day="3" isSelected={false} />
    <PlusDayButton />
  </View>
);

const DayButton = ({day = '1', isSelected = true}) => (
  <View style={{marginHorizontal: 8}}>
    {isSelected && (
      <ButtonElement
        type="clear"
        containerStyle={styles.deleteButtonContainer}
        buttonStyle={[styles.deleteButton, styles.shadow]}
        icon={<Icon type="foundation" name="x-circle" size={20} color="red" />}
      />
    )}
    <ButtonElement
      title={day}
      buttonStyle={[
        styles.dayButtonContainer,
        styles.shadow,
        {backgroundColor: isSelected ? Colors.blue2 : Colors.blue3},
      ]}
    />
  </View>
);

const PlusDayButton = () => (
  <ButtonElement
    type="clear"
    icon={<Icon type="entypo" name="plus" size={16} color={Colors.blue2} />}
    containerStyle={{marginHorizontal: 8}}
    buttonStyle={[styles.plusDayButton, styles.shadow]}
  />
);

const DetailButton = ({
  title = '123',
  textColor = 'grey',
  borderColor = 'grey',
  iconName,
  iconType,
  iconColor,
}) => {
  const _icon = () => (
    <Icon type={iconType} name={iconName} size={16} color={iconColor} />
  );

  return (
    <ButtonElement
      type="clear"
      title={title}
      style={[styles.detailButtonContainer, {borderColor}]}
      buttonStyle={styles.detailButton}
      titleStyle={[styles.detailButtonTitle, {color: textColor}]}
      icon={_icon}
    />
  );
};

/********************************************
    SUB LAYOUT - SEARCH
*********************************************/
interface ISearchViewProps {
  setSearchText: (any: string) => void;
  recentlySearchKeywordList: Array<string>;
  adjustKeywordList: (keyword: string) => void;
}

const SearchView = (props: ISearchViewProps) => {
  const {setSearchText, recentlySearchKeywordList, adjustKeywordList} = props;

  return (
    <View style={styles.searchViewContainer}>
      <View>
        <Text style={styles.recentlySearchTitle}>최근 검색어</Text>
        <View style={styles.titleMarkView}></View>
      </View>
      {recentlySearchKeywordList.map(keyword => (
        <SearchItem
          keyword={keyword}
          setSearchText={setSearchText}
          adjustKeywordList={adjustKeywordList}
        />
      ))}
    </View>
  );
};

const SearchItem = ({
  keyword = 'KEYWORD',
  setSearchText,
  adjustKeywordList,
}) => (
  <View style={styles.searchItemContainer}>
    <ButtonElement
      type="clear"
      title={keyword}
      style={styles.recentlySearchButtonContainer}
      containerStyle={{flex: 1}}
      titleStyle={styles.recentlySearchButtonTitle}
      buttonStyle={styles.recentlySearchButton}
      onPress={() => setSearchText(keyword)}
    />
    <ButtonElement
      type="clear"
      buttonStyle={styles.searchDeleteButton}
      onPress={() => adjustKeywordList(keyword)}
      icon={<Icon type="octicon" name="x" size={12} color="white" />}
    />
  </View>
);

/********************************************
  STYLE SHEETS
*********************************************/
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  topContainer: {
    height: 120,
    backgroundColor: '#3f6ba1',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  filterContainer: {
    width: '95%',
    height: 32,
    backgroundColor: 'white',
    borderRadius: 5,
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingHorizontal: 8,
    flexDirection: 'row',
  },

  searchBarContainer: {
    backgroundColor: '#3f6ba1',
  },
  inputContainer: {
    backgroundColor: 'white',
    color: 'black',
  },
  input: {
    backgroundColor: 'white',
    fontFamily: 'S-CoreDream-3Light',
    fontSize: 13,
    marginTop: 4,
  },
  inputIconContainer: {
    backgroundColor: 'transparent',
  },

  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
  },

  tripViewParentContainer: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  tripContainer: {
    width: '95%',
    height: 346,
    borderRadius: 8,
    marginTop: 16,
  },
  tripTitleContainer: {
    width: '100%',
    height: 40,
    backgroundColor: 'white',
    justifyContent: 'center',
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
  },
  tripViewTitle: {
    fontSize: 24,
    fontFamily: 'BlackHanSans-Regular',
    marginLeft: 16,
    marginTop: 10,
    marginBottom: 0,
    paddingBottom: 0,
  },
  carouselCountContainer: {
    position: 'absolute',
    top: 48,
    right: 8,
    width: 64,
    height: 24,
    borderRadius: 10,
    backgroundColor: '#33333380',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 1,
  },
  carouselCountText: {
    fontFamily: 'BlackHanSans-Regular',
    color: 'white',
  },
  detailContainer: {
    width: '100%',
    height: 32,
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 8,
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
  },
  detailButtonContainer: {
    height: 24,
    borderColor: 'grey',
    borderWidth: 1,
    borderRadius: 3,
    marginLeft: 8,
  },
  detailButton: {
    margin: 0,
    padding: 2,
    paddingHorizontal: 4,
  },
  detailButtonTitle: {
    fontFamily: 'BlackHanSans-Regular',
    fontSize: 14,
    marginLeft: 4,
    color: 'grey',
  },

  myTripBoxContainer: {
    width: '100%',
    height: 64,
    position: 'absolute',
    bottom: 0,
    backgroundColor: '#e0eaf6',
    flexDirection: 'row',
    paddingHorizontal: 16,
    paddingTop: 8,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: -8,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
  },
  dayButtonContainer: {
    margin: 0,
    padding: 0,
    width: 32,
    height: 32,
    borderRadius: 8,
  },
  deleteButtonContainer: {
    backgroundColor: 'white',
    borderRadius: 8,
    position: 'absolute',
    top: -6,
    right: -8,
    zIndex: 1,
  },
  deleteButton: {
    margin: 0,
    padding: 0,
    width: 16,
    height: 16,
    borderRadius: 8,
  },
  plusDayButton: {
    margin: 0,
    padding: 0,
    width: 32,
    height: 32,
    borderWidth: 1,
    borderColor: '#4273b3',
    backgroundColor: 'white',
    borderRadius: 8,
  },

  searchViewContainer: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 16,
    paddingTop: 16,
  },
  recentlySearchTitle: {
    fontFamily: 'BlackHanSans-Regular',
    fontSize: 24,
    zIndex: 1,
    color: 'black',
  },
  titleMarkView: {
    position: 'absolute',
    bottom: 4,
    left: -4,
    width: 120,
    height: 16,
    backgroundColor: '#3f6ba1',
    zIndex: 0,
  },
  searchItemContainer: {
    flexDirection: 'row',
    height: 40,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  recentlySearchButtonContainer: {
    flex: 1,
    alignItems: 'flex-start',
  },
  recentlySearchButtonTitle: {
    fontSize: 14,
    color: 'black',
  },
  recentlySearchButton: {
    flex: 1,
    margin: 0,
    padding: 0,
  },
  searchDeleteButton: {
    backgroundColor: '#3f6ba1',
    margin: 0,
    padding: 0,
    width: 16,
    height: 16,
    borderRadius: 8,
  },
});

export default TemplatePreview;
