import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  InteractionManager,
  TouchableOpacity,
} from 'react-native';
import {ISplashScreen} from '~/interfaces/Interfaces';
import Colors from '~/assets/Colors';
import Carousel from 'react-native-snap-carousel';
import {Avatar, Icon} from 'react-native-elements';
import StatusButton from "~/components/statusButton/StatusButton";

const ENTRIES1 = [
  {
    title: 'Beautiful and dramatic Antelope Canyon',
    subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
    illustration: 'https://i.imgur.com/UYiroysl.jpg',
  },
  {
    title: 'Earlier this morning, NYC',
    subtitle: 'Lorem ipsum dolor sit amet',
    illustration: 'https://i.imgur.com/UPrs1EWl.jpg',
  },
  {
    title: 'White Pocket Sunset',
    subtitle: 'Lorem ipsum dolor sit amet et nuncat ',
    illustration: 'https://i.imgur.com/MABUbpDl.jpg',
  },
  {
    title: 'Acrocorinth, Greece',
    subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
    illustration: 'https://i.imgur.com/KZsmUi2l.jpg',
  },
  {
    title: 'The lone tree, majestic landscape of New Zealand',
    subtitle: 'Lorem ipsum dolor sit amet',
    illustration: 'https://i.imgur.com/2nCt3Sbl.jpg',
  },
  {
    title: 'Middle Earth, Germany',
    subtitle: 'Lorem ipsum dolor sit amet',
    illustration: 'https://i.imgur.com/lceHsT6l.jpg',
  },
];

const TripDetail = (props: ISplashScreen) => {
  console.log('[TripDetail] props', props);

  const styles = StyleSheet.create({
    conatiner: {
      flex: 1,
      backgroundColor: Colors.white,
    },
  });

  return (
    <View style={styles.conatiner}>
      <TripPictureCarousel />
      <TripInfoView />
      <CommentItem />
      <CommentItem />
      <CommentItem />
      <View style={{height:40}} />
    </View>
  );
};

const CommentItem = () => {

  return (
      <View style={{ paddingHorizontal:16, flexDirection:'row', alignItems:'center', marginBottom:16}} >
        <Avatar
            size="small"
            rounded
            title="MT"
            onPress={() => console.log("Works!")}
            activeOpacity={0.7}
        />
        <View>
          <Text style={{marginLeft:16}} >
            코멘트를 입력해주세요
          </Text>
          <View style={{flexDirection:'row', marginLeft:8}} >
            <StatusButton
                content="123"
                iconName="like2"
                iconType="antdesign"
                height={16}
                fontSize={10}
                iconSize={10}
                iconColor={Colors.blue1}
                textColor={Colors.blue1}
                borderColor={Colors.blue1}
            />
            <StatusButton
                content="12"
                iconName="dislike2"
                iconType="antdesign"
                height={16}
                fontSize={10}
                iconSize={10}
                iconColor="red"
                textColor="red"
                borderColor="red"
            />
          </View>
        </View>
      </View>
  )
}

const TripPictureCarousel = () => {
  const {width: deviceWidth} = Dimensions.get('window');
  const sliderWidth = deviceWidth;
  const itemWidth = deviceWidth;

  const [currentIndex, setCurrentIndex] = useState(1);

  const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
  });

  const _renderItem = ({item, index}) => {
    return <Image style={{flex: 1}} source={{uri: item.illustration}} />;
  };

  return (
    <View style={styles.container}>
      <CarouselCountView
        currentIndex={currentIndex}
        maxLength={ENTRIES1.length}
      />
      <Carousel
        layout={'stack'}
        layoutCardOffset={18}
        data={ENTRIES1}
        renderItem={_renderItem}
        sliderWidth={sliderWidth}
        itemWidth={itemWidth}
        onSnapToItem={index => setCurrentIndex(index + 1)}
      />
    </View>
  );
};

interface ICarouselCountViewProps {
  currentIndex: number;
  maxLength: number;
}

const CarouselCountView = (props: ICarouselCountViewProps) => {
  const {currentIndex = 1, maxLength = 1} = props;

  const styles = StyleSheet.create({
    carouselCountContainer: {
      position: 'absolute',
      top: 16,
      right: 16,
      width: 64,
      height: 24,
      borderRadius: 10,
      backgroundColor: '#33333380',
      alignItems: 'center',
      justifyContent: 'center',
      zIndex: 1,
    },
    carouselCountText: {
      fontFamily: 'BlackHanSans-Regular',
      color: 'white',
    },
  });

  return (
    <View style={styles.carouselCountContainer}>
      <Text
        style={
          styles.carouselCountText
        }>{`${currentIndex} / ${maxLength}`}</Text>
    </View>
  );
};

const TripInfoView = () => {
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      padding: 24,
    },
    title: {
      fontSize: 48,
      fontFamily: 'BlackHanSans-Regular',
      zIndex: 1,
    },
    container2: {
      height: 100,
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
  });

  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.title}>제주공항</Text>
        <MarkLine />
      </View>
      <View style={styles.container2}>
        <InfoButton />
        <InfoButton />
        <InfoButton />
        <InfoButton />
      </View>
    </View>
  );
};

const MarkLine = () => (
  <View
    style={{
      position: 'absolute',
      bottom: 20,
      width: 160,
      height: 20,
      backgroundColor: Colors.blue3,
      zIndex: 0,
    }}
  />
);

const InfoButton = () => {
  const styles = StyleSheet.create({
    buttonContainer: {
      width: 100,
      height: 100,
    },
    container: {
      alignItems: 'center',
      justifyContent: 'center',
      marginTop: 16,
    },
    title: {
      fontSize: 16,
      fontFamily: 'BlackHanSans-Regular',
    },
  });

  return (
    <TouchableOpacity>
      <View style={styles.container}>
        <Icon type="ionicon" name="ios-call" size={48} />
        <Text style={styles.title}>전화걸기</Text>
      </View>
    </TouchableOpacity>
  );
};

export default TripDetail;
