const Colors = {
  white: '#FFFFFF',
  black: '#000000',

  blue1: '#3F6BA1',
  blue2: '#4273B3',
  blue3: '#A4C2E4',

  yellow1: '#f6c143',

  grey1: '#d9d9d9',
};

export default Colors;
