import * as React from 'react';
import {Button as ButtonElement} from 'react-native-elements';
import {useNavigation} from '@react-navigation/native';

export const LaunchRightButton = props => {
  console.log('[Component] launchRightButton props', props);

  const {navigation} = props;

  const plusIcon = {type: 'antdesign', name: 'pluscircleo', size: 24};
  const HIT_SLOP = {top: 16, right: 16, bottom: 16, left: 16};
  const BUTTON_STYLE = {marginRight: 8};

  return (
    <ButtonElement
      type="clear"
      icon={plusIcon}
      style={BUTTON_STYLE}
      hitSlop={HIT_SLOP}
      onPress={() => navigation.navigate('guideStack')}
    />
  );
};
