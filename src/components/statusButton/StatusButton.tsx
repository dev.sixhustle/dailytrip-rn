import React from 'react';
import {Platform, StyleSheet} from 'react-native';
import {Button as ButtonElement, Icon} from 'react-native-elements';

interface IStatusButtonProps {
  content: string;
  textColor: string;
  borderColor: string;
  iconName: string;
  iconType: string;
  iconColor: string;
}

const StatusButton = (props: IStatusButtonProps) => {
  console.log('[StatusButton] props', props);

  const {
    height = 24,
    fontSize = 14,
    iconSize = 16,
    content = '000,0000',
    textColor = 'grey',
    borderColor = 'grey',
    iconName,
    iconType,
    iconColor,
  } = props;

  const styles = StyleSheet.create({
    detailButtonContainer: {
      height: 24,
      borderColor: 'grey',
      borderWidth: 1,
      borderRadius: 3,
      marginLeft: 8,
    },
    detailButton: {
      margin: 0,
      padding: 2,
      paddingHorizontal: 4,
      borderWidth: Platform.OS === 'android' ? 1 : 0,
      marginLeft: Platform.OS === 'android' ? 8 : 0,
    },
    detailButtonTitle: {
      fontFamily: 'BlackHanSans-Regular',
      fontSize: 14,
      marginLeft: 4,
      color: 'grey',
    },
  });

  const _icon = () => (
    <Icon type={iconType} name={iconName} size={iconSize} color={iconColor} />
  );

  return (
    <ButtonElement
      type="clear"
      title={content}
      style={[styles.detailButtonContainer, {borderColor, height}]}
      buttonStyle={[styles.detailButton, {borderColor}]}
      titleStyle={[styles.detailButtonTitle, {color: textColor, fontSize}]}
      icon={_icon}
    />
  );
};

export default StatusButton;
